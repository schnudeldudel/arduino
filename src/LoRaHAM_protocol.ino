// Protokollbeispiel für "Hop" um Mehrfachsendungen zu vermeiden:
// [CALL1*RSSI#SNR:]~Hier kommt die Message die man senden will
//                                                             ^- hier wird normalerweise ein ASCII 0x00 eingefügt. Damit erkennt man ob der String nun fertig ist. Das geht halt nur bei Text. Sonst muss man die Informationslänge angeben
//                  ^- Hier muss die Länge der Nachricht als ASCII rein. Auch bei Text. Insgesammt darf 250 Byte aber nicht überschritten werden. Dadurch kann man erst Daten und nicht nur Text übertragen
//                 ^- Eckige Klammer zu sagt, Hop-Header geschlossen
//                ^- : Doppelpunkt schliest das Frame eines Teilnehmers. Doppelpunkt für alle. > Pfeil bedeutet einzellner Empfänger
//            ^- # Häschtag sagt nur: jetzt kommt SNR
//        ^- * Stern sagt nur, jetzt kommt RSSI -------- mit SNR und RSSI kann sich ein Empfänger einen starken Sender suchen, falls mehrere empfangen werden
// ^- [ Eckige Klammer offen sagt, jetzt kommt das Rufzeichen der Station
//
// [CALL wird vom Sender selbst angefügt. Ist er der Erste, schließt er das Frame mit : und ] ab.
// Wird hingegen das Frame von einem empfangen und soll weitergeleitet werden, so wird der RSSI und SNR angefügt und mit : und ] abgeschlossen.
// Das Frame sollte maximal 5 Rufzeichen enthalten.

// Beispiel Sender                    : [DC2WA/home:]§Dies ist eine Testnachricht
// dann kommt DB0ARD                  : [DC2WA/home*-112#5.25:DB0ARD:]§Dies ist eine Testnachricht
// Rene empfängt es                   : [DC2WA/home*-112#5.25:DB0ARD*-109#11.00:DG8RP:]§Dies ist eine Testnachricht
// Fabian empfängt es von Rene        : [DC2WA/home*-112#5.25:DB0ARD*-109#11.00:DG8RP*-103#8.50:DK2FK:]§Dies ist eine Testnachricht
// und Peter empfängt es zum senden   : [DC2WA/home*-112#5.25:DB0ARD*-109#11.00:DG8RP*-103#8.50:DK2FK*-64#22.25:DF7PE]§Dies ist eine Testnachricht
// nun empfängt es Fabian und Rene    :                                          ^^- kennt es    ^^- kennt es auch schon, Nachricht verwerfen, nicht weiter senden.
// wenn jetzt Peter noch ne PA hat und DB0ARD ihn hört:        ^^-- ahh, kenn ich schon!

// Alternativ:
// Beispiel Sender der zwei Repeater ausklammern will
//                                    : [DC2WA/home:DB0ARD:DK2FK:DF7PE:]§Dies ist eine Testnachricht
// nun werden diese Repeater die Nachricht nicht weiterleiten.

#include <string.h>
char ParsePattern[]="[]";

char* Hop="";
char* FrameMessage="";




void MakeHeader() {
}

void SetupLoRaModule() {
   
  Serial.println("LoRa Duplex - Set spreading factor");

  // override the default CS, reset, and IRQ pins (optional)
  LoRa.setPins(csPin, resetPin, irqPin); // set CS, reset, IRQ pin

  if (!LoRa.begin(LoRaFreqRX)) {             // initialize ratio 
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }

  LoRa.setFrequency(LoRaFreqRX);   // Change the frequency of the radio.
  LoRa.setSpreadingFactor(LoRaSF);           // ranges from 6-12,default 7 see API docs
  LoRa.setSignalBandwidth(LoRaBW);           // Supported values are 7.8E3, 10.4E3, 15.6E3, 20.8E3, 31.25E3, 41.7E3, 62.5E3, 125E3, 250E3, and 500E3. defaults to 125E3
  LoRa.setCodingRate4(5);  // Supported values are between 5 and 8,defaults to 5
  LoRa.setPreambleLength(8); // Supported values are between 6 and 65535. defaults to 8
  LoRa.setSyncWord(0x12); // byte value to use as the sync word, defaults to 0x12

  LoRa.enableCrc();
  // LoRa.disableCrc();  // Enable or disable CRC usage, by default a CRC is not used.

  LoRa.enableInvertIQ();
  LoRa.disableInvertIQ();  // Enable or disable Invert the LoRa I and Q signals, by default a invertIQ is not used.
  
  //LoRa.setGain(6);  // Supported values are between 0 and 6. If gain is 0, AGC will be enabled and LNA gain will not be used. Else if gain is from 1 to 6, AGC will be disabled and LNA gain will be used.
  
  LoRa.setTxPower(LoRaPower); // TX power in dB, defaults to 17
  Serial.println("LoRa init succeeded.");
}


void MakeBeacon() {
    if (millis() - lastSendTime > interval) {
    //LoRa.setSpreadingFactor(12);           // ranges from 6-12,default 7 see API docs
    String message = "["+Call+":]"+Beacon;   // send a message
    message += msgCount;
    sendMessage(message); 
    Serial.print("Sending '" + message);
#ifdef DEBUG
  // parse for a packet, and call onReceive with the result:
  Serial.print("'  Pwr : " + String(LoRaPower));
  Serial.print("  SF : " + String(LoRaSF));
  Serial.print("  BW : " + String(LoRaBW));
  Serial.println();
#endif
//  LoRaPower++;
//  LoRa.setTxPower(LoRaPower); // TX power in dB, defaults to 17
      //LoRaSF=12;
    //LoRa.setSpreadingFactor(LoRaSF);           // ranges from 6-12,default 7 see API docs
    
    lastSendTime = millis();            // timestamp the message
    interval = random(5000) + interval;    // 2-3 seconds
    msgCount++;
  }
}

String DecodeHop(String ParseFrame){
/* so sieht das Beispiel aus
String str("...");
char str_array[str.length()];
str.toCharArray(str_array, str.length());
char* token = strtok(str_array, " ");
*/
  
  char str[ParseFrame.length()+1];
  ParseFrame.toCharArray(str, ParseFrame.length()+1);
  char* ptr = strtok(str, ParsePattern);
  return ptr;

}

String DecodeMessage(String ParseFrame){

  char str[ParseFrame.length()+1];
  ParseFrame.toCharArray(str, ParseFrame.length()+1);
  char* ptr = strtok(str, ParsePattern);
  ptr = strtok(NULL, ParsePattern);
  return ptr;
 
}


boolean DecodeOwnCall(String ParseFrame){
String Own="";
String Oth="";
/*
String str("...");
char str_array[str.length()];
str.toCharArray(str_array, str.length());
char* token = strtok(str_array, " ");
*/
  
  char str[ParseFrame.length()+1];
  char OwnCall[Call.length()+1];
  ParseFrame.toCharArray(str, ParseFrame.length()+1);
  Call.toCharArray(OwnCall, Call.length()+1);
  char* ptr = strtok(str, "[]*#:");
  

  /*
  Serial.print (Own);
  Serial.print ("-");
  Serial.print (Oth);
  Serial.println ("-");
  */
  
  if (String(OwnCall) == String(ptr)){
  //  Serial.println (Oth);
  //  Serial.println ("^^-my Call");
    return true;
  }
  
  while (ptr != NULL)
  {
    ptr = strtok(NULL, "[]*#:");
    //Serial.println (ptr);
    //Oth=String(ptr);
    //Serial.println (Oth);
    
    if (String(OwnCall) == String(ptr)){
      //Serial.println ("^^--my Call");
      return true;
    }
  }

  return false;
}



void MakeTXProtocol() {

}


String InsertOwnCallToHop(String Message) {
  
  return "["+DecodeHop(Message)+Call+":]";

}
