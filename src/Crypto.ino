#include "mbedtls/aes.h"
#include "SPIFFSTest.h"
#include "Cipher.h"
#include <base64.h>
#include "crypto/base64.h"

CSPIFFS mSpiffs;
Cipher * cipher = new Cipher();

String AES256encode(char * key, String plainText) {
  cipher->setKey(key);    
  String text = cipher->encryptString(plainText);    
  return text;
}

String AES256decode(char * key, String plainText) {
  cipher->setKey(key);    
  String text = cipher->encryptString(plainText);    
  return text;
}

String Base64encode(String plainText){
  String toEncode = plainText;     
  String encoded = base64::encode(toEncode); 
  return encoded;
}

String Base64decode(String plainText){
  const char * toDecode = plainText.c_str(); //toCharArray(plainText);
  size_t outputLength;

  unsigned char * decoded = base64_decode((const unsigned char *)toDecode, strlen(toDecode), &outputLength);

  Serial.print("Length of decoded message: ");
  Serial.println(outputLength);

  Serial.printf("%.*s", outputLength, decoded);

    free(decoded);
 //     return String(decoded,outputLength);
}
