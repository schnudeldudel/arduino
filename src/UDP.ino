//https://www.arduino.cc/en/Tutorial/WiFiSendReceiveUDPString/

// MIT App Inventor:
//https://ullisroboterseite.de/android-AI2-UDP.html#exa

#include <WiFi.h>
//#include <WiFiUdp.h>
#include "AsyncUDP.h"

AsyncUDP udp;

unsigned int mylocalPort = 4040;      // local port to listen on


void SetupWiFi() {
    
    //WiFi.mode(WIFI_STA);
    //WiFi.begin(mySSID, myPASSWORD);
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
        Serial.println("WiFi Failed");
    } else{
      
           SendNetworkToLoRa();
    

      }          
    
}

void SendLoRaToNetwork(String Message) {
      if (WiFi.waitForConnectResult() != WL_CONNECTED) {
        Serial.println("WiFi Failed");
    } else{
  
  Serial.println("Send to LAN: "+ Message);
  char stringChar[Message.length()+1];
  Message.toCharArray(stringChar, Message.length());
  
  udp.broadcastTo(stringChar, mylocalPort);
  //free(stringChar);
  Message="";
  stringChar[1];
  //udp.broadcast("Anyone here?");
  //udp.broadcastTo("Anyone here? 4040", 4040);    
    }

}

void SendNetworkToLoRa() {
  
      if(udp.listen(mylocalPort)) {
            Serial.print("UDP Listening on IP: ");
            Serial.println(WiFi.localIP());
            
            udp.onPacket([](AsyncUDPPacket packet) {
            Serial.print("UDP Packet Type: ");
            Serial.print(packet.isBroadcast()?"Broadcast":packet.isMulticast()?"Multicast":"Unicast");
            Serial.print(", From: ");
            Serial.print(packet.remoteIP());
            Serial.print(":");
            Serial.print(packet.remotePort());
            Serial.print(", To: ");
            Serial.print(packet.localIP());
            Serial.print(":");
            Serial.print(packet.localPort());
            Serial.print(", Length: ");
            Serial.print(packet.length());
            Serial.print(", Data: ");
            Serial.write(packet.data(), packet.length());
            Serial.println();
            //reply to the client
            packet.printf("Got %u bytes of data", packet.length());
            delay(10);
            
            // sudo make LoRa
            message = "["+Call +":]";

            String UDPString = String( (char*) packet.data());
            //serialMessage=String(packet.data());
            SendTime=millis();
            sendMessage(message+UDPString);
            
             #ifdef DEBUG
                  Serial.print("OK");
                  Serial.print(" Time: ");
                  Serial.print(millis() - SendTime);
                  Serial.println("ms");
             #endif
             
             UDPString="";
             message="";
             //SetupLoRaModule();
            });
      }
  
}
