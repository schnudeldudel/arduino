#include <Wire.h>

#ifdef SSD1306
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#endif

#ifdef TFT35
 #include "TFT_eSPI.h"
 
TFT_eSPI tft = TFT_eSPI();                   // Invoke custom library with default width and height
#define CENTRE 240
#endif

/*
#include <SPI.h>
#include "Ucglib.h"
*/

#ifdef SSD1306
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
#endif

 // Ucglib_ILI9486_18x320x480_SWSPI ucg(/*sclk=*/ 22, /*data=*/ 21,/*cd=*/ 5, /*cs=*/ 13, /*reset=*/ 4);
 //  Ucglib_ILI9486_18x320x480_HWSPI ucg(/*cd=*/ 5, /*cs=*/ 13, /*reset=*/ 4);


// TFT_eSPI myGLCD = TFT_eSPI();       // Invoke custom library


void SSD1306init() {
#ifdef SSD1306
  pinMode(14, OUTPUT);    // sets the digital pin 13 as output
  digitalWrite(14, HIGH); // sets the digital pin 13 on
  pinMode(27, OUTPUT);    // sets the digital pin 13 as output
  digitalWrite(27, LOW); // sets the digital pin 13 on
  delay(100);
  
  Wire.begin(17,16); 
  
   // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    //for(;;); // Don't proceed, loop forever
  }else{
    

   /*
   */

  /*
// Setup the LCD
  myGLCD.init();
  myGLCD.setRotation(1);
  myGLCD.fillRect(0, 0, 319, 14,TFT_RED);
*/
  
/*

  ucg.begin(UCG_FONT_MODE_TRANSPARENT);
  ucg.setFont(ucg_font_ncenR14_hr);
  ucg.clearScreen();

  ucg.setPrintPos(25,45);
  ucg.setPrintDir(0);
  ucg.print("Ucg");
*/


  // Clear the buffer
  display.clearDisplay();

  // Draw a single pixel in white
  //display.drawPixel(10, 10, SSD1306_WHITE);

  // Show the display buffer on the screen. You MUST call display() after
  // drawing commands to make them visible on screen!
  display.display();

  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);     // Start at top-left corner
  display.cp437(true);         // Use full 256 char 'Code Page 437' font
  display.println("Gualtherius");
  
  display.setTextSize(2);
  //display.setTextColor(BLACK, WHITE); // 'inverted' text
  display.setTextColor(WHITE, BLACK);
  display.setCursor(0,14);
  display.println("LoRaHAM.de");
  display.display();
  }
  #endif

  
#ifdef TFT35
  // ILI9486
  tft.init();
  tft.setRotation(3);
  tft.fillScreen(TFT_BLACK);
  tft.drawCentreString("Gualherius", CENTRE, 3, 1);

  //tft.setTextColor(TFT_YELLOW,TFT_GREY);
  tft.drawCentreString("LoRaHAM.de", CENTRE, 309,1);

  tft.setCursor(0, 10, 2);    // Set cursor to x = 76, y = 150 and use font 4  
 #endif
}

void putDisplay(String msg){
 // Clear the buffer
 #ifdef SSD1306
  display.clearDisplay();
  
  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE, SSD1306_BLACK); // Draw white text
  display.setCursor(0, 0);     // Start at top-left corner
  display.cp437(true);         // Use full 256 char 'Code Page 437' font
  display.println(msg);
  
  display.display();
#endif  
}

void setTX(){
 #ifdef SSD1306
  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE, SSD1306_BLACK); // Draw white text
  display.setCursor(110, 0);     // Start at top-left corner
  display.println("TX");
  display.display();
#endif  
}

void delTX(){
 #ifdef SSD1306
  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE, SSD1306_BLACK); // Draw white text
  display.setCursor(110, 0);     // Start at top-left corner
  display.println("  ");
  display.display();
#endif   
}

void invertDisplay( bool i){
 #ifdef SSD1306  
    display.invertDisplay(i);
 #endif      
}
