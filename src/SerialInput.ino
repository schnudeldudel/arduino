String serialMessage = "";  
String message = "";        
long SendTime = 0;        // time of last packet send

void serialLoop() {
  while (Serial.available() > 0) {
    byte read = Serial.read();

    if (read == '\n') {
      Serial.println("UART in: " + serialMessage);
//      message = serialMessage;
      // Message msg(call, serialMessage);
      message = "["+Call +":]";

      //sendMessage(message);
      //delay(100);
      

  
      #ifdef DEBUG
      Serial.print("Header: "+message);
      Serial.println(" Message: "+serialMessage);
            
  
      Serial.print(" Sending...");
      #endif
     
      SendTime=millis();

      sendMessage(message+serialMessage);

      #ifdef DEBUG
      Serial.print("OK");
      Serial.print(" Time: ");
      Serial.print(millis() - SendTime);
      Serial.println("ms");
      #endif
      
      serialMessage="";
      continue;
    }
   
    if (read > 0 && serialMessage.length() < 140) {
      serialMessage += (char) read;
    }
    
  }
}
