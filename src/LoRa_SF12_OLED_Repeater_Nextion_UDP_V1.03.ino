/*
  
  This code is designed for Gualtherius LoRaHAM. Pinout and PCB for 1 Watt of freedom.

  here comes some cool text in future.

  IDE    : Arduino 1.8.12
  Lib    : arduino-LoRa 0.7.2 by sandeepmistry              https://github.com/sandeepmistry/arduino-LoRa
         : Adafruit_SSD1306 2.4.0
         : Adafruit-GFX-Library 1.10.2
         : ESP32 OTA                                        https://github.com/SensorsIot/ESP32-OTA
  ESP32  : 1.0.4                                            https://dl.espressif.com/dl/package_esp32_index.json
  Board  : ESP32 Dev Module
  Target : Gualtherius LoRaHAM ESP32 V1.00

  created 04.11.2020
  by Alexander Walter

  based on LoRa from Tom Igoe

  https://github.com/sandeepmistry/arduino-LoRa/blob/master/API.md

  Source: Amateurfunk\LoRaHAM\LoRaOTA\LoRa_OTA\LoRa_OTA.ino (zu Hause)
  Changelog:
  11.08.2020 OTA added (Attention: if you have OTA active, ESP32 trys to connect to AP. If no AP to connect, ESP32 wont start! Remove "setupOTA" to prevent this)
  
*/
#include <SPI.h>              // include libraries
#include <LoRa.h>

#include "Gualtherius_LoRaHAM_Config.h"
#include "NextionSerialInput.h"

const String Call = "DL0TEST";   // Callsign
const String Beacon = "Test-Beacon. vy73 ";   // Beacon


// #define Repeater // Aktiviere die Repeaterfunktion
#define DEBUG
// #define WLAN
// #define OTA
//#define Nextion
//#define Display
//#define SSD1306
// #define TFT35
// #define TFT28

//#define BAKENMODE

#ifdef OTA
#include "OTA.h"
#include "credentials.h"
#endif


#ifdef Nextion
 
#define RXD2 16
#define TXD2 17

#endif



//https://github.com/oe3cjb/TTGO-T-Beam-LoRa-APRS/tree/master/src


int LoRaFreqRX= 433900E3;  // https://aprs.at/index.php/2018/02/06/lora-aprs-frequenzumstellung-am-12-2-2018/
int LoRaFreqTX= 433775E3;
/*
*/

/*
int LoRaFreqRX= 433775E3;
int LoRaFreqTX= 433775E3;
*/
/*
int LoRaFreqRX= 434100E3;  // Simplex Frequenz
int LoRaFreqTX= 434100E3;


int LoRaFreqRX= 439700E3;  // Frequenz für DL0ARD-LoRa-Digi
int LoRaFreqTX= 434900E3;
*/

int LoRaSF= 12;
int LoRaBW= 125E3;   // Supported values are 7.8E3, 10.4E3, 15.6E3, 20.8E3, 31.25E3, 41.7E3, 62.5E3, 125E3, 250E3, and 500E3. defaults to 125E3
int LoRaPower = 20;


word msgCount = 0;            // count of outgoing messages
int interval = 60000;          // interval between sends
long lastSendTime = 0;        // time of last packet send

int buttonState = 0;


void setup() {

  Serial.begin(9600);                   // initialize serial
  #ifdef Nextion
    Serial1.begin(9600, SERIAL_8N1, RXD2, TXD2);
  #endif
 
  while (!Serial);
  pinMode(0, INPUT);

 
 // setupOTA("Gualtherius LoRaHAM JW34", mySSID, myPASSWORD);
 
 #ifdef WLAN
  #ifdef OTA
  setupOTA("Gualtherius LoRaHAM DC2WA/m", mySSID, myPASSWORD);
  #endif
  SetupWiFi();
 #endif
 
  ESPinfo();  // Give me Info about ESP32
  SetupLoRaModule();

 // 
  
  // register the receive callback
  // LoRa.onReceive(onReceive);
  // put the radio into receive mode
  // LoRa.receive();


  SSD1306init();

  Serial.println(Base64encode(AES256encode("abcdefghijklmnoo","Test von für Base64"))); //fixed output to BAse64

}

void loop() {
#ifdef defined(ESP32_RTOS) && defined(ESP32)
#else // If you do not use FreeRTOS, you have to regulary call the handle method.
 #ifdef OTA
  ArduinoOTA.handle();
 #endif
#endif


  buttonState = digitalRead(0);

  if (buttonState == LOW){
    Serial.println("GPIO 0 pressed...");
    Serial.println("Message: RESET");
    sendMessage("RESET");
    delay(10);
  }

/*  
   if (buttonState == LOW){
    Serial.println("GPIO 0 pressed...");
    Serial.println("Message: DC2WA-0>ARPS:!4813.62N/01539.85E_.../...g...t-000r...p...P...h00b......DHT22");
    sendMessage("DC2WA-0>ARPS:!4813.62N/01539.85E_.../...g...t-000r...p...P...h00b......DHT22");
    delay(100);
  }
*/  

  onReceive(LoRa.parsePacket());

  serialLoop();

  // Serial.println("idle...");
  #ifdef BAKENMODE
  MakeBeacon();
  #endif
  
  #ifdef Nextion  
  HMI_display_rec();
  #endif
}




void sendMessage(String outgoing) {
  //delay(random(1000));
  //onReceive(LoRa.parsePacket());
  //Serial.println("Setting LoRa TX-Freq: "+String(LoRaFreqTX));
  LoRa.setFrequency(LoRaFreqTX);   // Change the frequency of the radio.
  setTX();
  //  delay(100);
  
  LoRa.beginPacket();                   // start packet
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  // LoRa.receive();                     // go back into receive mode
  //msgCount++;                           // increment message ID
  MyLastMessage(outgoing);
  //putDisplay(outgoing);
  delTX();
  //Serial.println("Setting LoRa RX-Freq: "+String(LoRaFreqRX));
  LoRa.setFrequency(LoRaFreqRX);   // Change the frequency of the radio.


}

void onReceive(int packetSize) {
  //byte dato[200];
  if (packetSize == 0) return;          // if there's no packet, return
// Invert and restore display, pausing in-between
  invertDisplay(true);
  Serial.println("Incoming LoRa.");

  // read packet header bytes:
  String incoming = "";

  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }
  
  /*
  delay(100);
  
  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }
  */
  
  //incoming.toCharArray(dato, incoming.length())


  Serial.print("RX-Header: " + DecodeHop(incoming));
  Serial.println(" Message: " + DecodeMessage(incoming));
 
  //Serial2.println(" Message: " + DecodeMessage(incoming));
  #ifdef Nextion
  Serial1.print("page2.monitor.txt=\"" + DecodeMessage(incoming)+"\"");
  #endif

  #ifdef Display
  putDisplay(incoming);
  #endif

  #ifdef ToLAN
  SendLoRaToNetwork(DecodeHop(incoming)+DecodeMessage(incoming));
  #endif
  //Serial.println( CRC8( dato  , dato.length() ) );
#ifdef DEBUG
 
  Serial.print("RSSI: " + String(LoRa.packetRssi()));
  Serial.print(" Snr: " + String(LoRa.packetSnr()));
  Serial.println(" freqErr: " + String(LoRa.packetFrequencyError()));
  Serial.println();
#endif


  delay(random(1000));
  if (LoRa.available()) {
   return;
  }else{
    
    #ifdef Repeater
      PlayRepeater(incoming);
    #endif
  }
   invertDisplay(false);
}



byte CRC8( const byte *data, byte len ) {

  byte crc = 0x00;
  while (len--) {

    byte extract = *data++;
    for (byte tempI = 8; tempI; tempI--) {
      byte sum = (crc ^ extract) & 0x01;
      crc >>= 1;
      if (sum) {
        crc ^= 0x8C;
      }
      extract >>= 1;
    }
  }
  return crc;
}

/*
void testdrawchar(void) {
  display.clearDisplay();

  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(WHITE); // Draw white text
  display.setCursor(0, 0);     // Start at top-left corner
  display.cp437(true);         // Use full 256 char 'Code Page 437' font

  // Not all the characters will fit on the display. This is normal.
  // Library will draw what it can and the rest will be clipped.
  for(int16_t i=0; i<256; i++) {
    if(i == '\n') display.write(' ');
    else          display.write(i);
  }

  display.display();
  delay(2000);
}
*/
